# Mac Development Ansible Playbook

This repository is a fork of Jeff Geerlings excellent repository: https://github.com/geerlingguy/mac-dev-playbook
It is a stripped version suited to my needs.

For installation:
 
    $ xcode-select --install
    $ sudo easy_install pip
    $ sudo pip install ansible
    $ mkdir -p ~/code/src/gitlab.com
    $ cd ~/code/src/gitlab.com
    $ git clone git@gitlab.com:skift/os_playbook.git
    $ cd os_playbook
    $ ansible-galaxy install -r requirements.yml
    $ ansible-playbook -i inventory --ask-sudo-password main.yml
    $ cd ~/dotfiles
    $ bin/install
    $ bin/setup_osx