
install:
	ansible-galaxy install -r requirements.yml

run:
	ansible-playbook -i inventory --become-user=shawn main.yml
